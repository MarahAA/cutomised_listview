package jo.edu.ju.customlistview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var list = mutableListOf<Model>()

        list.add(Model("Flower" , "Flowers are plants. wacky, i know.", R.drawable.ic_filter_vintage_black_24dp ))
        list.add(Model("Sun Flares" , "They're hot. Proven Scientifically", R.drawable.ic_flare_black_24dp ))
        list.add(Model("Free Breakfast" , "only for 9.99$", R.drawable.ic_free_breakfast_black_24dp ))
        list.add(Model("Stars" , "Stars are Lit. *-*-* ", R.drawable.ic_grade_black_24dp ))
        list.add(Model("Hot Tubs" , "Kill people. Read more.", R.drawable.ic_hot_tub_black_24dp ))
        list.add(Model("Smile" , " 'Uhm, you should smile more' - famous last words", R.drawable.ic_insert_emoticon_black_24dp ))

        mListView.adapter = MyListAdapter(this, R.layout.raw_list_view, list)
    }
}
